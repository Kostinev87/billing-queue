<?php
/**
 * Created by PhpStorm.
 * User: Evgenii Kostin
 * Date: 09.03.2019
 * Time: 1:33
 */

namespace App;


class Transaction
{
    const TYPE_ADD = 1;
    const TYPE_SUB = 2;
    const TYPE_MOVE = 3;

    protected $_allowedTypes = [
        self::TYPE_ADD,
        self::TYPE_SUB,
        self::TYPE_MOVE,
    ];

    protected $_type = 0;
    protected $_data = [];

    public function __construct(int $type, array $data)
    {
        if (in_array($type, $this->_allowedTypes, true)) {
            $this->_type = $type;
            $this->_data = $data;
        }
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->_type;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->_data;
    }
}