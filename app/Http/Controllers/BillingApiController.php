<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessBilling;
use App\Transaction;

class BillingApiController extends Controller
{
    public function add(int $userId, float $money)
    {
        $transaction = new Transaction(
            Transaction::TYPE_ADD,
            [
                'user_id' => $userId,
                'money' => $money
            ]
        );

        dispatch(new ProcessBilling($transaction));

        return '{success:true}';
    }

    public function sub(int $userId, float $money)
    {
        $transaction = new Transaction(
            Transaction::TYPE_SUB,
            [
                'user_id' => $userId,
                'money' => $money
            ]
        );

        dispatch(new ProcessBilling($transaction));

        return '{success:true}';
    }

    public function move(int $userIdFrom, int $userIdTo, float $money)
    {
        $transaction = new Transaction(
            Transaction::TYPE_MOVE,
            [
                'user_id_from' => $userIdFrom,
                'user_id_to' => $userIdTo,
                'money' => $money
            ]
        );

        dispatch(new ProcessBilling($transaction));

        return '{success:true}';
    }
}
