<?php

namespace App\Jobs;

use App\Transaction;

class ProcessTransactionResults extends Job
{
    /**
     * @var Transaction
     */
    protected $_transaction;
    protected $_error = '';

    /**
     * ProcessTransactionResults constructor.
     * @param Transaction $transaction
     * @param string $error
     */
    public function __construct(Transaction $transaction, string $error = '')
    {
        $this->_transaction = $transaction;
        $this->_error = $error;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // здесь мы обрабатываем результаты транзакции
    }
}
