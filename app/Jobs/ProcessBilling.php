<?php

namespace App\Jobs;

use App\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class ProcessBilling extends Job
{
    /**
     * @var Transaction
     */
    protected $_transaction;

    /**
     * ProcessBilling constructor.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->_transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $type = $this->_transaction->getType();
        if (!$type) {
            return;
        }

        $result = false;
        $data = $this->_transaction->getData();
        DB::beginTransaction();
        switch ($type) {
            case Transaction::TYPE_ADD:
                $result = $this->_add(
                    $data['user_id'] ?? 0,
                    $data['money'] ?? 0.0
                );
                break;
            case Transaction::TYPE_SUB:
                $result = $this->_sub(
                    $data['user_id'] ?? 0,
                    $data['money'] ?? 0.0
                );
                break;
            case Transaction::TYPE_MOVE:
                $result = $this->_move(
                    $data['user_id_from'] ?? 0,
                    $data['user_id_to'] ?? 0,
                    $data['money'] ?? 0.0
                );
                break;
            default:
                break;
        }

        if ($result) {
            DB::commit();
            dispatch(new ProcessTransactionResults($this->_transaction, 'Transaction not complete'));
            return;
        }

        DB::rollBack();
        dispatch(new ProcessTransactionResults($this->_transaction));
        return;
    }

    /**
     * @param int $userIdFrom
     * @param int $userIdTo
     * @param float $money
     * @return bool
     */
    protected function _move(int $userIdFrom, int $userIdTo, float $money): bool
    {
        if (!$userIdFrom || !$userIdTo || !$money || ($money < 0)) {
            return false;
        }

        $balances = $this->_getUsersBalance([$userIdFrom, $userIdTo]);

        if (!$balances) {
            return false;
        }

        $userIdToBalanceMap = [];
        foreach ($balances as $balance) {
            $userIdToBalanceMap[$balance->user_id] = $balance->balance;
        }

        if (!isset($userIdToBalanceMap[$userIdFrom], $userIdToBalanceMap[$userIdTo])) {
            return false;
        }

        $newBalanceFrom = $userIdToBalanceMap[$userIdFrom] - (int) ($money * 100);
        if ($newBalanceFrom < 0) {
            return false;
        }

        $newBalanceTo = $userIdToBalanceMap[$userIdTo] + (int) ($money * 100);
        $this->_updateUserBalance($userIdFrom, $newBalanceFrom);
        $this->_updateUserBalance($userIdTo, $newBalanceTo);

        return true;
    }

    /**
     * @param int $userId
     * @param float $money
     * @return bool
     */
    protected function _add(int $userId, float $money): bool
    {
        if (!$userId || !$money || ($money < 0)) {
            return false;
        }

        $balance = $this->_getUserBalance($userId);
        if (!$balance) {
            return false;
        }

        $newBalance = $balance + (int) ($money * 100);
        $this->_updateUserBalance($userId, $newBalance);
        return true;
    }

    /**
     * @param int $userId
     * @param float $money
     * @return bool
     */
    protected function _sub(int $userId, float $money): bool
    {
        if (!$userId || !$money || ($money < 0)) {
            return false;
        }

        $balance = $this->_getUserBalance($userId);
        if (!$balance) {
            return false;
        }

        $newBalance = $balance - (int) ($money * 100);
        if ($newBalance < 0) {
            return false;
        }

        $this->_updateUserBalance($userId, $newBalance);
        return true;
    }

    protected function _getUserBalance(int $id): int
    {
        return (int) DB::table('user_balance')
            ->where('user_id', $id)
            ->value('balance');
    }

    protected function _getUsersBalance(array $ids): Collection
    {
        return DB::table('user_balance')
            ->whereIn('user_id', $ids)
            ->get();
    }

    protected function _updateUserBalance(int $id, int $balance): int
    {
        return DB::table('user_balance')
            ->where('user_id', $id)
            ->update(['balance' => $balance]);
    }
}
